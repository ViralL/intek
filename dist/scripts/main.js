'use strict';

// ready
$(document).ready(function() {

    //date calendar
    $('.date').pickmeup_twitter_bootstrap({
        position       : 'right',
        format: 'd B Y',
        hide_on_select : true
    });
    $('.date-range').pickmeup_twitter_bootstrap({
        position       : 'right',
        mode : 'range',
        format: 'd B Y',
        hide_on_select : true
    });
    //date calendar

    //// anchor
    //$(".anchor").on("click","a", function (event) {
    //    event.preventDefault();
    //    var id  = $(this).attr('href'),
    //        top = $(id).offset().top;
    //    $('body,html').animate({scrollTop: top + 40}, 1000);
    //});
    //// anchor

    // adaptive menu
    $('.main-nav__toggle--js').click(function () {
        $(this).next().slideToggle();
    });
    // adaptive menu

    // mask phone {maskedinput}
    //$("[name=phone]").mask("+7 (999) 999-9999");
    // mask phone

    // slider {slick-carousel}
    //$('.slider').slick({});
    // slider

    // select {select2}
    function formatState (state) {
        if (!state.id) { return state.text; }
        var $state = $(
            '<span><span class="select-img"><img src="images/icons/' + state.element.value.toLowerCase() + '.png" /></span>' + state.text + '<span>'
        );
        return $state;
    }
    $('.select-hard').on("select2:select", function(e) {
        var classSelect = $(this).parent().parent().find('.select2');
        var cusSelect = $(this).parent().find('.select2-selection__rendered').attr('title');
        if (  cusSelect == '' ) {
            classSelect.removeClass('edited');
        } else {
            classSelect.addClass('edited');
        }
    });
    $('select').select2({
        minimumResultsForSearch: Infinity
    });
    $('select.select-actions').select2({
        minimumResultsForSearch: Infinity,
        templateResult: formatState,
        templateSelection: formatState,
        placeholder: 'Выберите действие'
    });
    // select

    $('.select-actions').on("select2:select", function(e) {
        //var classSelect = $(this).parent().parent().find('.select2');
        var cusSelect = $(this).parent().find('.select2-selection__rendered').attr('title');
        if (  cusSelect == '' ) {
            $('.btn-actions').attr('disabled', true)
        } else {
            $('.btn-actions').attr('disabled', false);
        }
    });

    //.close--js
    $('.close--js').click(function () {
       $(this).parent().hide();
    });
    //.close--js


    // Floating labels
    var handleInput = function(el) {
        if (el.val() != "") {
            el.addClass('edited');
        } else {
            el.removeClass('edited');
        }
    };
    //
    $('body').on('keydown', '.form-control', function(e) {
        handleInput($(this));
    });
    $('body').on('blur', '.form-control', function(e) {
        handleInput($(this));
    });

    $('.dropdown-menu .form__group').click(function(e) {
        e.stopPropagation();
    });

    $('.popover-main').click(function () {
        $(this).popover('show');
        return false;
    });
    $('body').on('click', function (e) {
        //did not click a popover toggle or popover
        if ($(e.target).data('toggle') !== 'popover'
            && $(e.target).parents('.popover.in').length === 0) {
            $('[data-toggle="popover"]').popover('hide');
        }
    });

    // popup {magnific-popup}
    //$('.image-gallery').magnificPopup({});
    // popup



    //search
    $('.mobile--btn').click(function () {
        $(this).prev().addClass('active');
    });
    //search

    //footer
    $('.footer-menu .title').click(function () {
        $(this).parent().toggleClass('active');
    });
    //footer

    //.input__file-js
    $('.input__file-js').change(function() {
        $('.input__file-js').each(function() {
            var name = this.value;
            var reWin = /.*\\(.*)/;
            var fileTitle = name.replace(reWin, "$1");
            var reUnix = /.*\/(.*)/;
            fileTitle = fileTitle.replace(reUnix, "$1");
            //$(this).parent().parent().find('.input__name-js').val(fileTitle);
            //$(this).parent().find('.btn_js').text("Готово");
            //$(this).parent().find('.btn').text("Готово");
            $('.input__text-js').text('Загружено: '+ fileTitle);
        });
    });
    //.input__file-js

    //sitemap
    $('.submenu--js').click(function () {
        $(this).toggleClass('active')
        return false;
    });
    //sitemap

    //.slider-line--js
    $('.slider-line--js').each(function(){
        var per = $(this).attr('data-per');
        $(this).children().css('width', per + '%');
        if ( per > 70 ) {
            $(this).children().css('background-color','#0063b4');
        } else if ( per > 40 && per <= 70) {
            $(this).children().css('background-color','#00b5ff');
        } else {
            $(this).children().css('background-color','#acd1e6');
        }
    });
    //.slider-line--jsi

    //table accordion
    $('.acc-title--js').click(
        function() {
            $(this).toggleClass('active').parent().parent().next().toggle();
        }
    );
    //table accordion

    $('.search-adv--js').click( function () {
        $(this).parents().find('.catalog-tah__hide').slideToggle();
        return false;
    });


    //tabs
    $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });
    $('.radiotab--js').on('click', '.radio input:not(.active)', function () {
        var radTab = $(this).val();
        if (radTab == 'deliver') {
            $(this).parent().parent().addClass('active');
            $(this).parent().parent().next().removeClass('active');
            $(this).closest('.radiotab').find('.radiotab--content-1').addClass('active');
            $(this).closest('.radiotab').find('.radiotab--content-2').removeClass('active');
            $(this).closest('.radiotab').find('.radiotab--content-3').removeClass('active');
        } else if (radTab == 'card') {
            $(this).parent().parent().addClass('active');
            $(this).parent().parent().prev().removeClass('active');
            $(this).parent().parent().next().removeClass('active');
            $(this).closest('.radiotab').find('.radiotab--content-1').removeClass('active');
            $(this).closest('.radiotab').find('.radiotab--content-2').addClass('active');
            $(this).closest('.radiotab').find('.radiotab--content-3').removeClass('active');
        } else {
            $(this).parent().parent().addClass('active');
            $(this).parent().parent().prev().removeClass('active');
            $(this).closest('.radiotab').find('.radiotab--content-1').removeClass('active');
            $(this).closest('.radiotab').find('.radiotab--content-2').removeClass('active');
            $(this).closest('.radiotab').find('.radiotab--content-3').addClass('active');
        }
    });
    //tabs


    //show pass
    $(".user-pass").each(function (index, input) {
        var $input = $(input);
        $(".show-pass--js").click(function () {
            var change = "";
            if ($(this).html() === '&nbsp;<i class="fa fa-eye-slash"></i>&nbsp;') {
                $(this).html('&nbsp;<i class="fa fa-eye"></i>&nbsp;');
                change = "text";
            } else {
                $(this).html('&nbsp;<i class="fa fa-eye-slash"></i>&nbsp;');
                change = "password";
            }
            var rep = $("<input type='" + change + "' />")
                .attr("id", $input.attr("id"))
                .attr("name", $input.attr("name"))
                .attr('class', $input.attr('class'))
                .val($input.val())
                .insertBefore($input);
            $input.remove();
            $input = rep;
        });
    });
    //show pass
});
// ready


//# sourceMappingURL=main.js.map